#!/bin/bash

usage=\
"Usage: $(basename "$0") [OPTION]...\n
Build Krita in the current container\n
\n
where:\n
    -h,      --help              show this help text\n
    -jN,     --jobs=N            use N parallel jobs, default value is \`nproc\`\n
    -r,      --run               run Krita after building\n
    -n,      --no-pch            disable the usage of PCH headers
    -t,      --no-tests          disable building of tests
             --krita-asan        build Krita with address sanitizer active\n
             --deps              build Krita deps\n
             --qt-asan           enable address sanitizer for Qt (implies --qt-debug)\n
             --qt-debug          enable debugging info for Qt\n
"

RUN_KRITA=0
JOBS=`nproc`
BUILD_DEPS=0
QT_DEBUG=0
QT_ASAN=0
ENABLE_PCH=1
ENABLE_TESTS=1


# Call getopt to validate the provided input. 
options=$(getopt -o tnrj:h --long no-tests,no-pch,run,jobs:,help,krita-asan,deps,qt-asan,qt-debug -- "$@")
[ $? -eq 0 ] || { 
    echo "Incorrect options provided"
    exit 1
}
eval set -- "$options"
while true; do
    case "$1" in
    -r | --run)
        RUN_KRITA=1
        ;;
    -j | --jobs)
        shift;
        JOBS=$1
        ;;
    -n | --no-pch)
        ENABLE_PCH=0
        ;;
    -t | --no-tests)
        ENABLE_TESTS=0
        ;;
    -h | --help)
        echo -e $usage >&2
        exit 1
        ;;
    --krita-asan)
        KRITA_EXTRA_CMAKE_OPTIONS="-DECM_ENABLE_SANITIZERS=\'address\'"
        ;;
    --deps)
        BUILD_DEPS=1
        ;;
    --qt-asan)
        QT_ASAN=1
        BUILD_DEPS=1
        ;;
    --qt-debug)
        QT_DEBUG=1
        BUILD_DEPS=1
        ;;
    --)
        shift
        break
        ;;
    esac
    shift
done

KRITA_EXTRA_CMAKE_OPTIONS="$KRITA_EXTRA_CMAKE_OPTIONS -DKRITA_ENABLE_PCH=$ENABLE_PCH -DBUILD_TESTING=$ENABLE_TESTS"

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source ${DIR}/find_default_container_file.inc

container_name=$(findContainerName)
if [ -z ${container_name} ]; then
    exit 1
fi

tryStartContainer $container_name

if [ $BUILD_DEPS -eq 1 ]; then
    if [ $QT_DEBUG -eq 1 ]; then
        DEPS_CMAKE_ENV="QT_ENABLE_DEBUG_INFO=1"
    fi

    if [ $QT_ASAN -eq 1 ]; then
        DEPS_CMAKE_ENV+="QT_ENABLE_ASAN=1"
    fi

    ${DOCKER_BINARY} exec -ti ${container_name} \
        bash -i -c "cd /home/appimage/appimage-workspace/ && $DEPS_CMAKE_ENV ~/persistent/krita/packaging/linux/appimage/build-deps.sh ~/appimage-workspace ~/persistent/krita" || exit 1
fi

${DOCKER_BINARY} exec -ti ${container_name} \
        bash -i -c "cd /home/appimage/appimage-workspace/krita-build && if [[ ! -f CMakeCache.txt ]]; then ~/bin/run_cmake.sh ${KRITA_EXTRA_CMAKE_OPTIONS} ~/persistent/krita; fi" || exit 2

if [ $RUN_KRITA -eq 0 ]; then
    ${DOCKER_BINARY} exec -ti ${container_name} \
        bash -i -c "cd /home/appimage/appimage-workspace/krita-build && make -j$JOBS install" || exit 3
else
    ${DOCKER_BINARY} exec -ti ${container_name} \
        bash -i -c "cd /home/appimage/appimage-workspace/krita-build && make -j$JOBS install && krita" || exit 3
fi
